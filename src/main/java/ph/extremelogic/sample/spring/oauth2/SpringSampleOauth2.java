package ph.extremelogic.sample.spring.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "ph.extremelogic")
public class SpringSampleOauth2 extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SpringSampleOauth2.class, args);
    }

}
